require("dotenv").config();
const http = require('http'),
    routes = require('./routes/index'),
    express = require('express'),
    app = express(),
    server = require("http").createServer(app),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    path = require('path'),
    io = require('socket.io')(server, { cors: { origin: '*' } });
const port = 3000;
const message = require('./utils/messages')
const users = require('./utils/users')
const history = require('./utils/history')
var textHistory = -1;
const router = express.Router();

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: false })) // for parsing application/x-www-form-urlencoded
app.use(express.static(path.join(__dirname, "public")));

// session
app.use(session({
    secret: "secret",
    saveUninitialized: true,
    resave: true
}));

// Define view engine
app.set("view engine", "pug");

// Define routes using URL path
app.use("/", routes);

server.listen(port, (err, res) => {
    if (err) console.log(`ERROR: Connecting APP ${err}`);
    else console.log(`Server is running on port ${port}`);
});

// socket config
io.on("connection", (socket) => {

    socket.on('joinRoom', ({ username, sala }) => {
        const user = users.userJoin(socket.id, username, sala)
        socket.join(user.room)
        //Quando un usuari es conecta
        socket.broadcast.to(user.room).emit('message', message.formatMessage('ivanBot', `${user.username} has joined the chat`));
        // envia informacio de usuari i de la sala
        io.to(user.room).emit('roomUsers', {
            room: user.room,
            users: users.getRoomUsers(user.room)
        })
    })
    //console.log("Nuevo cliente");
    //Bienvenido nuevo usuario
    socket.emit("connected", {
        msg: "Bienvenido al chat de Ivan",
    });

    //Este socket tendrá un atributo user con valor Pedro,
    //desde la primera conexión hasta que se cierre el socket
    if (!socket.user) socket.user = session.username;

    //Todos los clientes que estén escuchando el evento "toChat" recibiran el mensaje enviado por el cliente que lanzó el mensaje
    /* socket.on("broadcast", (data) => {
        socket.broadcast.emit("toChat", { msg: data });
    }); */

    // Escolta per a chatMessage
    socket.on('chatMessage', (msg) => {
        const user = users.getCurrentUser(socket.id);
        // envia a todos los conectados el mensaje i de quien es
        io.to(user.room).emit('message', message.formatMessage(user.username, msg));
        // añadimos todos los mensajes que llegan al servidor para crear un historial
        textHistory = history.addMsg(user.username + ': ' + msg, user.room);
    })

    //Quando un usuari se desconecta
    socket.on('disconnect', () => {
        try {
            const user = users.userLeave(socket.id);
            io.emit('message', message.formatMessage('ivanBot', `${user.username} has left the chat`));
            io.to(user.room).emit('roomUsers', {
                room: user.room,
                users: users.getRoomUsers(user.room)
            })

        } catch (e) {
            console.log('Error encontrando al user' + e);
        }

    })
});

app.use(express.json());
app.use(express.static(__dirname));

// Connect to mongoose

mongoose.connect(
    `mongodb://devroot:devroot@mongo:27017/nodechat?authSource=admin`,
    { useNewUrlParser: true },
    (err, res) => {
        if (err) console.log(`ERROR: connecting to Database.  ${err}`);
        else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);

// rutas de history

router.get('/history/list', function (req, res) {
    res.render('history');
});
router.get('/history/view', function (req, res) {
    res.render('historyView')
});
