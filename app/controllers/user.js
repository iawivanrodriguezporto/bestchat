const User = require('../models/user');

//Create a new user and save it
var add = async (req, res) => {
    console.log(req.body)
    var user = await new User({ username: req.body.username, password: req.body.password });
	await user.save();
	res.redirect('/');
	return user;
};

//find all people
var list = (req, res, next) => {
	User.find(function (err, users) {
		return users;
	});
};

//find person by id
var find = (req, res) => {
	User.findOne({ _id: req.params.id }, function (error, user) {
		return user;
	})
};

var logout = (req, res) => {
	delete req.session.userId;
	req.session.destroy();
	res.redirect('/');
}

var login = async (req, res) => {
	const user = await User.findOne({username: req.body.username});
	if (!user) {
		return res.status(404).json({message: 'username does not exist'});
	}
	if (user.password != req.body.password) {
		return res.status(401).json({message: 'password incorrect'});
	}
	req.session.username = req.body.username;
	//res.json({message: 'loged in', username: user.username, password: user.password});
    res.render('chatList');
}

var loginRequired = async (req, res, next) => {
	if (!req.session || !req.session.userId) {
		return res.status(403).json({message: 'you must login'});
	}
	req.user = await User.findById(req.session.userId);
	if (!req.user) {
		return res.status(403).json({message: 'this user id no longer exists'});
	}
	next();
}

var chatSelected = (req, res) => {
	res.render('views/chatView');
}

module.exports = {
	add,
	list,
	find,
	logout,
	login,
	loginRequired,
	chatSelected
}