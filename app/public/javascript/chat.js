document.addEventListener("DOMContentLoaded", function (event) {
    //Nos conectamos al servicio de socket
    const socket = io("http://192.168.1.46:3000");
    const userMessage = document.getElementById("userMessage");
    const chatBox = document.getElementById("chatText");
    const username = document.getElementById('username').value;
    const sala = document.getElementById('sala').value;
    const userConnected = document.getElementById('userConnected');
    let msgInput;
    /*
    * Acciones que se realizan cuando se establece conexión con el servidor de socket.
    */
    socket.on("connected", (data) => {
        console.log(data.msg + ' ' + username);
        userConnected.append();
    });

    /**
     * Unirse al chatRoom
     */
    socket.emit('joinRoom', { username, sala });

    /**
     * Get room and users
     */
    socket.on('roomUsers', ({ room, users }) =>{
        document.getElementById('room').innerHTML = ' Sala ' + room;
        let message = '';
        users.forEach(element => {  
           message += element.username + "\n";
        });
        userConnected.innerHTML =  message;
    })

    /**
     * Message from server
     */
    socket.on('message', message => {
        console.log(message)
        chatBox.append(message.time  + ' ' + message.username + " : " + message.text + "\n");
        chatBox.scrollTop = chatBox.scrollHeight;
    })

    /**
     * Acciones cuando se pulsa el botón de "Enviar"
     */
    document.getElementById("send").addEventListener("submit", (e) => {
        e.preventDefault();
        msgInput = userMessage.value;

        // deixem el container per escriure buit
        userMessage.value = "";
        let msg = msgInput;

        socket.emit('chatMessage', msg);
        chatBox.scrollTop = chatBox.scrollHeight;
    });
});
