const express = require('express')
const router = express.Router();
const userController = require('../controllers/user');
const history = require('../utils/history');

router.get('/', (req, res) => {
  res.render('index')
})
router.get('/chatList', function (req, res) {
  res.render('chatList')
});

router.post('/chatView', function (req, res) {
  res.render('chatView', { username: req.session.username, sala: req.body.btnradio });
});

router.get('/register', function (req, res) {
  res.render('register');
});

router.get('/chatSelect', userController.loginRequired, userController.chatSelected);

router.get('/history/list', function (req, res) {
  res.render('history');
});
router.post('/history/view', history.passVar ,function (req, res) {
  let text;
  if (req.params.roomText.get(req.body.room) == undefined) {
    text = [''];
  } else {
    text = req.params.roomText.get(req.body.room);
  }
  res.render('historyView', { roomText: text, room: req.body.room })
});

router.post('/deleteHistory', history.deleteMsgRoom, history.passVar, (req, res) => {
  console.log( req.params.roomText);
  res.render('historyView', { roomText: req.params.roomText, room: req.body.room })
})

//Link routes and functions
// user routes

router.get('/users', userController.list);
router.get('/user/:id', userController.find);
router.get('/logout', userController.logout);
router.post('/user/login', userController.login);
router.post('/user/register', userController.add);

module.exports = router;
