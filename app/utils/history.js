const roomText = new Map();
var allRoomMsg = new Array();
// afegir el text a un mapa clau = sala, valor text

function addMsg(msg, room) {
    if (roomText.get(room) == undefined) {
        allRoomMsg = new Array();
    } else {
        allRoomMsg = roomText.get(room);
    }
    allRoomMsg.push(msg); 
    roomText.set(room, allRoomMsg);
    return roomText;
}

// afegir el text a un req.params

let passVar = (req, res, next) => {
    req.params.roomText = roomText;
    next();
}

let deleteMsgRoom = (req, res, next) => {
    console.log(req.body.room);
    roomText.delete(req.body.room)
    roomText.set(req.body.room, ['']);
    next()
}

module.exports = {
    addMsg,
    passVar,
    deleteMsgRoom
}