const users = [];

// Join user to chat

function userJoin(id, username, room){
    const user = { id, username, room };
    users.push(user);
    return user;
}

function getCurrentUser(id) {
    return users.find(user => user.id === id)
}

function userLeave(id) {
    const pos = users.findIndex(user => user.id === id);
    if (pos !== -1) {
        return users.splice(pos, 1)[0];
    }
}

// Get room users
function getRoomUsers(room) {
    return users.filter(user => user.room == room);
}
module.exports = {
    userJoin,
    getCurrentUser,
    userLeave,
    getRoomUsers
}